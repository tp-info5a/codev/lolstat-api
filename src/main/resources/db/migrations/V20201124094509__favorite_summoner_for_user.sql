create table favorite_summoner
(
    id int auto_increment,
    user_id int not null,
    summoner_name varchar(255) not null,
    summoner_icon int not null default 685,

    constraint favorite_summoner_pk primary key (id),
    constraint favorite_summoner_user_fk foreign key (user_id) references user(id)
)
