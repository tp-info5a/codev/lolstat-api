create table user
(
	id int auto_increment,
	first_name varchar(255) null,
	last_name varchar(255) null,
	email varchar(255) not null,

	constraint user_pk primary key (id),
	constraint user_uk_email unique (email)
);

create table user_password
(
    user_id int auto_increment,
    password varchar(255) null,

    constraint user_password_pk primary key (user_id),
    constraint user_password_user_kf foreign key (user_id) references user(id)
);
