create table favorite_champion
(
    id int auto_increment,
    user_id int not null,
    champion_id varchar(255) not null,

    constraint favorite_champion_pk primary key (id),
    constraint favorite_champion_user_fk foreign key (user_id) references user(id)
)
