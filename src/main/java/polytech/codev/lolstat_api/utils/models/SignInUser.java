package polytech.codev.lolstat_api.utils.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class SignInUser {
    private String mail;
    private String password;
}
