package polytech.codev.lolstat_api.utils.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChampionRotationResponse {
    private List<Integer> freeChampionIds;
    private List<Integer> freeChampionIdsForNewPlayers;
    private Integer maxNewPlayerLevel;

    public ChampionRotationResponse() {}
}
