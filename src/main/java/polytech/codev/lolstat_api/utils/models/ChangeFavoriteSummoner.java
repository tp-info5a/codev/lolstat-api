package polytech.codev.lolstat_api.utils.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ChangeFavoriteSummoner {
    private String summonerName;
    private Integer summonerIcon;
    private Boolean addToFavorite;
}
