package polytech.codev.lolstat_api.utils.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;

@NoArgsConstructor
@Getter
@Setter
public class SummonerMasteriesUtils {
    private Integer championId;
    private Integer championLevel;
    private Integer championPoints;
    private BigInteger lastPlayTime;
    private Integer championPointsSinceLastLevel;
    private Integer championPointsUntilNextLevel;
    private Boolean chestGranted;
    private Integer tokensEarned;
    private String summonerId;
}
