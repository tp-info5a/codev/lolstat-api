package polytech.codev.lolstat_api.utils.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class RegisterUser {
    private String firstName;
    private String lastName;
    private String email;
    private String password;
}
