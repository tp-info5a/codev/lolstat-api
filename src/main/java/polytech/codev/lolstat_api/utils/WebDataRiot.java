package polytech.codev.lolstat_api.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.Exceptions.ApiExceptionEnum;
import polytech.codev.lolstat_api.entities.*;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class WebDataRiot {

    private final Logger logger = LoggerFactory.getLogger(WebDataRiot.class);

    @Value("${riot.data}")
    private String riotDataUrl;
    @Value("${riot.data.version}")
    private String riotVersion;
    @Value("${riot.data.language}")
    private String riotLanguage;
    @Value("${riot.data.champion.all}")
    private String riotDataChampionListUrl;
    @Value("${riot.data.champion.info}")
    private String riotDataChampionInfoUrl;
    @Value("${riot.data.champion.square}")
    private String riotDataChampionSquare;
    @Value("${riot.data.champion.splash}")
    private String riotDataChampionSplash;
    @Value("${riot.data.champion.image.passive}")
    private String riotDataChampionPassiveImage;
    @Value("${riot.data.champion.image.spell}")
    private String riotDataChampionSpellImage;
    @Value("${riot.data.item}")
    private String riotDataItem;
    @Value("${riot.data.item.image}")
    private String riotDataItemImage;

    public WebDataRiot() {}

    @Cacheable(value = "allChampions")
    public List<Champion> getAllChampionFromRiot() throws ApiException {
        this.logger.info("Get all champion");

        String url = this.riotDataUrl + this.riotVersion + this.riotLanguage + this.riotDataChampionListUrl;
        JSONObject json = ReadJsonFromUrl.read(url);

        return json.getJSONObject("data")
                .toMap()
                .entrySet()
                .stream()
                .map(stringObjectEntry ->
                        new Champion(
                                Integer.parseInt(((HashMap) stringObjectEntry.getValue()).get("key").toString()),
                                stringObjectEntry.getKey(),
                                this.riotDataUrl + this.riotVersion + this.riotDataChampionSquare + stringObjectEntry.getKey() + ".png"
                        )
                )
                .sorted(Comparator.comparing(Champion::getName))
                .collect(Collectors.toList());
    }

    public ChampionData getChampionByName(String championName) throws ApiException {
        this.logger.info("Get info for : " + championName);

        String url = this.riotDataUrl + this.riotVersion + this.riotLanguage + this.riotDataChampionInfoUrl + championName + ".json";
        JSONObject json = ReadJsonFromUrl.read(url);

        ChampionData championData = new Gson().fromJson(json.getJSONObject("data").getJSONObject(championName).toString(), ChampionData.class);

        championData.setImageUrl(this.riotDataUrl + this.riotVersion + this.riotDataChampionSquare + championName + ".png");
        championData.getPassive().setImageUrl(
                this.riotDataUrl + this.riotVersion + this.riotDataChampionPassiveImage
                + json.getJSONObject("data").getJSONObject(championName).getJSONObject("passive").getJSONObject("image").getString("full")
        );

        championData.getSpells()
            .forEach(championDataSpell ->
                championDataSpell.setImageUrl(this.riotDataUrl + this.riotVersion + this.riotDataChampionSpellImage + championDataSpell.getId() + ".png ")
            );

        championData.getSkins()
                .forEach(championDataSkin ->
                    championDataSkin.setImageUrl(this.riotDataUrl + this.riotDataChampionSplash + championName + "_" + championDataSkin.getNum() + ".jpg")
                );

        List<ItemForChampion> collect = championData.getRecommended()
                .stream()
                .filter(itemForChampion -> !(itemForChampion.getMode().equalsIgnoreCase("ARAM") || itemForChampion.getMode().equalsIgnoreCase("CLASSIC")))
                .collect(Collectors.toList());

        championData.getRecommended().removeAll(collect);

        String itemUrl = this.riotDataUrl + this.riotVersion + this.riotLanguage + this.riotDataItem;
        JSONObject itemJson = ReadJsonFromUrl.read(itemUrl);

        Iterator<ItemForChampion> itemForChampionIterator = championData.getRecommended().iterator();
        if (itemForChampionIterator.hasNext()) {
            do {
                ItemForChampion itemForChampion = itemForChampionIterator.next();

                Iterator<ItemBlock> itemBlockIterator = itemForChampion.getBlocks().iterator();
                if (itemBlockIterator.hasNext()) {
                    do {
                        ItemBlock block = itemBlockIterator.next();

                        Iterator<Item> itemIterator = block.getItems().iterator();
                        if (itemIterator.hasNext()) {
                            do {
                                Item item = itemIterator.next();

                                try {
                                    this.completeItem(itemJson, item);
                                } catch (ApiException e) {
                                    itemIterator.remove();
                                }
                            } while (itemIterator.hasNext());
                        }
                    } while (itemBlockIterator.hasNext());
                }
            } while (itemForChampionIterator.hasNext());
        }

        return championData;
    }

    public void completeItem(JSONObject itemListJson, Item item) throws ApiException {
        Integer id = item.getId();
        try {
            JSONObject dataItemJson = itemListJson.getJSONObject("data").getJSONObject(String.valueOf(id));
            Item tmp = new Gson().fromJson(dataItemJson.toString(), Item.class);
            item.completeItem(tmp);
            item.setImageUrl(this.riotDataUrl + this.riotVersion + this.riotDataItemImage + id + ".png");
        } catch (Exception e) {
            throw new ApiException(404, ApiExceptionEnum.API_404_ITEMNOTFOUND, id.toString());
        }

    }


    public Item getItemById(Integer id) throws ApiException {
        if (id == 0)
            return new Item();

        String itemUrl = this.riotDataUrl + this.riotVersion + this.riotLanguage + this.riotDataItem;
        JSONObject itemJson = ReadJsonFromUrl.read(itemUrl).getJSONObject("data");

        Optional<Item> optionalItem = itemJson.keySet()
                .stream()
                .filter(s -> id.equals(Integer.parseInt(s)))
                .map(s -> {
                    JSONObject jsonObject = itemJson.getJSONObject(s);
                    Item item = new Item();
                    item.setId(Integer.parseInt(s));
                    item.setName(jsonObject.getString("name"));
                    item.setDescription(jsonObject.getString("description"));

                    ItemGold itemGold = new ItemGold(
                            jsonObject.getJSONObject("gold").getInt("base"),
                            jsonObject.getJSONObject("gold").getInt("total")
                    );
                    item.setGold(itemGold);
                    item.setImageUrl(this.riotDataUrl + this.riotVersion + this.riotDataItemImage + item.getId() + ".png");

                    return item;
                }).findFirst();

        if (optionalItem.isEmpty())
            throw new ApiException(404, ApiExceptionEnum.API_404_ITEMNOTFOUND, id.toString());

        return optionalItem.get();
    }

    public Champion getChampionById(Integer championId) throws ApiException {
        return this.getAllChampionFromRiot().stream().filter(champion -> champion.getId().equals(championId)).findFirst().get();
    }

    public String getMapById(Integer id) throws ApiException {
        JSONArray mapJSON = ReadJsonFromUrl.readArray("http://static.developer.riotgames.com/docs/lol/maps.json");

        Optional<String> stringOptional = mapJSON.toList()
                .stream()
                .filter(o -> ((HashMap<String, Object>) o).get("mapId") == id)
                .map(o -> ((HashMap<String, Object>) o).get("mapName").toString()).findFirst();

        return stringOptional.get();
    }
}
