package polytech.codev.lolstat_api.utils.jwt;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.entities.ApiUser;
import polytech.codev.lolstat_api.services.ServiceUser;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtRequestFilter extends OncePerRequestFilter {

    private final ServiceUser serviceUser;
    private final JwtUtil jwtUtil;

    public JwtRequestFilter(ServiceUser serviceUser, JwtUtil jwtUtil) {
        this.serviceUser = serviceUser;
        this.jwtUtil = jwtUtil;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        final String requestTokenHeader = request.getHeader("Authorization");
        String username = null;
        String jwtToken = null;

        // JOn attend le token sous la forme  "Bearer token"
        if (requestTokenHeader != null && requestTokenHeader.startsWith("Bearer ")) {
            jwtToken = requestTokenHeader.substring(7);
            try {
                username = this.jwtUtil.getUsernameFromToken(jwtToken);
            } catch (IllegalArgumentException e) {
                System.out.println("Unable to get JWT Token");
            } catch (ExpiredJwtException e) {
                System.out.println("JWT Token has expired");
            }

        } else {
            logger.warn("JWT Token does not begin with Bearer String");
        }

        // le token est valide.
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            ApiUser apiUser;
            UserDetails userDetails;
            try {
                apiUser = this.serviceUser.loadUserByUsername(username);
                userDetails = this.jwtUtil.loadUser(apiUser);
            } catch (ApiException e) {
                e.printStackTrace();
                throw new ServletException("User not found");
            }
            // Si le token est valide on configure la sécurité de Spring Security t
            if (this.jwtUtil.validateToken(jwtToken, userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(
                        userDetails,
                        null,
                        userDetails.getAuthorities()
                );
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                // L'utilisateur est authentifié, et il a pssé la sécurité de Spring
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }
        chain.doFilter(request, response);
    }
}
