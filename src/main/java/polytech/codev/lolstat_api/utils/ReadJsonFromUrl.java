package polytech.codev.lolstat_api.utils;

import org.json.JSONArray;
import org.json.JSONObject;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.Exceptions.ApiExceptionEnum;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class ReadJsonFromUrl {

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject read(String url) throws ApiException {
        try {
            InputStream is = new URL(url).openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            is.close();
            return json;
        } catch (IOException e) {
            throw new ApiException(500, ApiExceptionEnum.API_500_CANTREADDATAFROMRIOT, url);
        }
    }

    public static JSONArray readArray(String url) throws ApiException {
        try {
            InputStream is = new URL(url).openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            String jsonText = readAll(rd);
            JSONArray json = new JSONArray(jsonText);
            is.close();
            return json;
        } catch (IOException e) {
            throw new ApiException(500, ApiExceptionEnum.API_500_CANTREADDATAFROMRIOT, url);
        }
    }
}
