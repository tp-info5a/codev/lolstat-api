package polytech.codev.lolstat_api.utils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.Exceptions.ApiExceptionEnum;
import polytech.codev.lolstat_api.entities.*;
import polytech.codev.lolstat_api.utils.models.ChampionRotationResponse;
import polytech.codev.lolstat_api.utils.models.SummonerMasteriesUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class WebServiceRiot {

    private final Logger logger = LoggerFactory.getLogger(WebServiceRiot.class);

    @Value("${riot.api}")
    private String riotApiUrl;
    @Value("${riot.api.summoner.by-name}")
    private String riotApiSummonerByNameUrl;
    @Value("${riot.api.champion.rotation}")
    private String riotApiChampionRotationUrl;
    @Value("${riot.api.token}")
    private String riotApiToken;
    @Value("${riot.api.masteries.by-id}")
    private String riotApiMasteriesByIdUrl;
    @Value("${riot.api.history.by.id}")
    private String riotApiHistoryByAccountIdUrl;
    @Value("${riot.data.summoner.icon}")
    private String riotDataSummonerIcon;
    @Value("${riot.api.detail.match}")
    private String riotDetailMatch;

    private HttpHeaders createHttpHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Accept-Language", "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7");
        httpHeaders.set("Accept-Charset", "application/x-www-form-urlencoded; charset=UTF-8");
        httpHeaders.set("X-Riot-Token", riotApiToken);
        return httpHeaders;
    }

    public Summoner getSummonerByName(String name) throws ApiException {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = this.createHttpHeader();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<String> exchange = null;
        try{
            exchange = restTemplate.exchange(this.riotApiUrl + this.riotApiSummonerByNameUrl + name,HttpMethod.GET,httpEntity,String.class);
        }catch (HttpClientErrorException e) {
           if (e.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
               throw new ApiException(404,ApiExceptionEnum.API_404_NOSUMMONERFOUND, name);
           }else if (e.getStatusCode().equals(HttpStatus.TOO_MANY_REQUESTS)) {
               throw new ApiException(429,ApiExceptionEnum.API_429_TOOMANYREQUEST);
           } else {
               throw new ApiException(503,ApiExceptionEnum.API_503_CANTCONNECTTORIOT);
           }
        }catch (Exception e){
            e.printStackTrace();
            throw new ApiException(503,ApiExceptionEnum.API_503_CANTCONNECTTORIOT);
        }
        String body = exchange.getBody();
        if(body == null){
            throw new ApiException(500, ApiExceptionEnum.API_500_DATANOTREADABLE);
        }
        Type summonerType = new TypeToken<Summoner>(){}.getType();
        return new Gson().fromJson(body, summonerType);
    }

    public List<SummonerMasteries> getMasteryBySummonerId(String summonerId, List<Champion> allChampionFromRiot) throws ApiException{
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = this.createHttpHeader();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<String> exchange;
        System.out.println(this.riotApiUrl + this.riotApiMasteriesByIdUrl + summonerId);
        try{
            exchange =  restTemplate.exchange(
                    this.riotApiUrl + this.riotApiMasteriesByIdUrl + summonerId,
                    HttpMethod.GET,
                    httpEntity,
                    String.class
            );
        } catch (Exception e){
            e.printStackTrace();
            throw new ApiException(503, ApiExceptionEnum.API_503_CANTCONNECTTORIOT);
        }

        String body = exchange.getBody();

        if (body == null) {
            throw new ApiException(500, ApiExceptionEnum.API_500_DATANOTREADABLE);
        }

        Type summonerMasteriesListType = new TypeToken<ArrayList<SummonerMasteriesUtils>>(){}.getType();

        List<SummonerMasteriesUtils> summonerMasteriesUtilsList = new Gson().fromJson(body, summonerMasteriesListType);

        summonerMasteriesUtilsList = summonerMasteriesUtilsList.stream().limit(5).collect(Collectors.toList());

        List<Integer> collect = summonerMasteriesUtilsList.stream()
                .map(SummonerMasteriesUtils::getChampionId)
                .collect(Collectors.toList());

        List<SummonerMasteriesUtils> finalSummonerMasteriesUtilsList = summonerMasteriesUtilsList;
        List<SummonerMasteries> summonerMasteriesList = allChampionFromRiot.stream()
                .filter(champion -> collect.contains(champion.getId()))
                .map(champion -> new SummonerMasteries(
                                champion,
                                finalSummonerMasteriesUtilsList.stream()
                                        .filter(summonerMasteriesUtils -> summonerMasteriesUtils.getChampionId().equals(champion.getId()))
                                        .findFirst()
                                        .get()
                        )
                )
                .sorted(Comparator.comparing(SummonerMasteries::getChampionPoints).reversed())
                .collect(Collectors.toList());
        return summonerMasteriesList;
    }

    @Cacheable(value = "freeChampionRotation")
    public List<Integer> getFreeChampionId() throws ApiException {
        this.logger.info("Get free champions rotation");

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = this.createHttpHeader();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);
        ResponseEntity<ChampionRotationResponse> exchange;
        try {
            exchange = restTemplate.exchange(
                    this.riotApiUrl + this.riotApiChampionRotationUrl,
                    HttpMethod.GET,
                    httpEntity,
                    ChampionRotationResponse.class
            );
        } catch (Exception e) {
            throw new ApiException(503, ApiExceptionEnum.API_503_CANTCONNECTTORIOT);
        }


        return Objects.requireNonNull(exchange.getBody()).getFreeChampionIds();
    }

    public Page<Game> getHistoryByAccountId(String accountId, Integer page) throws ApiException {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = this.createHttpHeader();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<String> exchange;
        System.out.println(this.riotApiUrl + this.riotApiHistoryByAccountIdUrl + accountId);
        try{
            exchange =  restTemplate.exchange(
                    this.riotApiUrl + this.riotApiHistoryByAccountIdUrl + accountId,
                    HttpMethod.GET,
                    httpEntity,
                    String.class
            );
        } catch (Exception e){
            e.printStackTrace();
            throw new ApiException(503, ApiExceptionEnum.API_503_CANTCONNECTTORIOT);
        }

        String body = exchange.getBody();

        if (body == null) {
            throw new ApiException(500, ApiExceptionEnum.API_500_DATANOTREADABLE);
        }

        Type MatchListType = new TypeToken<Matches>(){}.getType();

        Matches matches = new Gson().fromJson(body, MatchListType);

        Pageable pageableRequest = PageRequest.of(page,matches.getMatches().size());

        Page<Game> gamePage = new PageImpl<>(matches.getMatches().subList(5 * (page - 1), 5 * page), pageableRequest, matches.getMatches().size());

        return gamePage;
    }

    public MatchDto getInfoMatch(Long id) throws ApiException{
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders httpHeaders = this.createHttpHeader();
        HttpEntity<?> httpEntity = new HttpEntity<>(httpHeaders);

        ResponseEntity<String> exchange;
        System.out.println(this.riotApiUrl + this.riotDetailMatch + id);
        try{
            exchange =  restTemplate.exchange(
                    this.riotApiUrl + this.riotDetailMatch + id,
                    HttpMethod.GET,
                    httpEntity,
                    String.class
            );
        } catch (Exception e){
            e.printStackTrace();
            throw new ApiException(503, ApiExceptionEnum.API_503_CANTCONNECTTORIOT);
        }
        String body = exchange.getBody();

        if (body == null) {
            throw new ApiException(500, ApiExceptionEnum.API_500_DATANOTREADABLE);
        }

        Type MatchDetail = new TypeToken<MatchDto>(){}.getType();

        return new Gson().fromJson(body,MatchDetail);
    }
}
