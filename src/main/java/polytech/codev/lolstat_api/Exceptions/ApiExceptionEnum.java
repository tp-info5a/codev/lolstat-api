package polytech.codev.lolstat_api.Exceptions;

public enum ApiExceptionEnum {
    API_404_NOSUMMONERFOUND("No summoner found for "),
    API_404_ITEMNOTFOUND("Can't find the item with id : "),

    API_429_TOOMANYREQUEST("Too many request, please wait a moment."),

    API_500_CANTREADDATAFROMRIOT("Can't read data from "),
    API_500_DATANOTREADABLE("The data from RIOT Games are not readable."),

    API_503_CANTCONNECTTORIOT("Can't connect to RIOT Games API."),
    API_500_USEREMAILNOTUNIQUE("This email address is already register, please choose another or try to log in."),
    API_500_USERNOTFOUND("No user found for this email address."),
    API_500_BADPASSWORD("Wrong password, try again.");

    private String message;

    ApiExceptionEnum(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
