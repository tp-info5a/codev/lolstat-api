package polytech.codev.lolstat_api.Exceptions;

import org.springframework.http.HttpStatus;

public class ApiException extends Exception {
    private Integer status;

    public ApiException(Integer status, String message) {
        super(message);
        this.status = status;
    }

    public ApiException(Integer status, ApiExceptionEnum exceptionMessageEnum) {
        this(status, exceptionMessageEnum.getMessage());
    }

    public ApiException(Integer status, ApiExceptionEnum exceptionMessageEnum, String details) {
        this(status, exceptionMessageEnum.getMessage() + details);
    }

    public Integer getStatus() {
        return status;
    }
}
