package polytech.codev.lolstat_api.services;

import org.springframework.stereotype.Service;
import polytech.codev.lolstat_api.entities.Champion;

import java.util.List;

@Service
public class ServiceChampion {
    public ServiceChampion() {}

    public void getFreeChampionFromIdList(List<Champion> championList, List<Integer> freeChampionIdList) {
        championList.stream()
                .filter(champion -> freeChampionIdList.contains(champion.getId()))
                .forEach(champion -> champion.setFree(true));
    }
}
