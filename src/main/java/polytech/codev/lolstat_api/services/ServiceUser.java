package polytech.codev.lolstat_api.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.Exceptions.ApiExceptionEnum;
import polytech.codev.lolstat_api.entities.*;
import polytech.codev.lolstat_api.repositories.RepositoryApiUser;
import polytech.codev.lolstat_api.repositories.RepositoryFavoriteChampion;
import polytech.codev.lolstat_api.repositories.RepositoryFavoriteSummoner;
import polytech.codev.lolstat_api.utils.models.ChangeFavoriteChampion;
import polytech.codev.lolstat_api.utils.models.ChangeFavoriteSummoner;
import polytech.codev.lolstat_api.utils.models.RegisterUser;
import polytech.codev.lolstat_api.utils.models.SignInUser;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ServiceUser {

    private final Logger logger = LoggerFactory.getLogger(ServiceUser.class);

    private final RepositoryApiUser repositoryUser;
    private final RepositoryFavoriteChampion repositoryFavoriteChampion;
    private final RepositoryFavoriteSummoner repositoryFavoriteSummoner;

    private final PasswordEncoder passwordEncoder;

    public ServiceUser(RepositoryApiUser repositoryUser, PasswordEncoder passwordEncoder, RepositoryFavoriteChampion repositoryFavoriteChampion, RepositoryFavoriteSummoner repositoryFavoriteSummoner) {
        this.repositoryUser = repositoryUser;
        this.passwordEncoder = passwordEncoder;
        this.repositoryFavoriteChampion = repositoryFavoriteChampion;
        this.repositoryFavoriteSummoner = repositoryFavoriteSummoner;
    }

    public ApiUser registerNewUser(RegisterUser registerUser) throws ApiException {
        logger.info("register new user : "+registerUser.getEmail());

        List<ApiUser> allByEmail = this.repositoryUser.findAllByEmail(registerUser.getEmail());

        if (!allByEmail.isEmpty())
            throw new ApiException(500, ApiExceptionEnum.API_500_USEREMAILNOTUNIQUE);

        registerUser.setPassword(passwordEncoder.encode(registerUser.getPassword()));

        ApiUser user = new ApiUser(registerUser);

        return this.repositoryUser.save(user);
    }

    public ApiUser signInUser(SignInUser signInUser) throws ApiException {
        Optional<ApiUser> optionalUser = this.repositoryUser.findAllByEmail(signInUser.getMail()).stream().findFirst();

        if (optionalUser.isEmpty())
            throw new ApiException(500, ApiExceptionEnum.API_500_USERNOTFOUND);

        String encodedPassword = optionalUser.get().getUserPassword().getPassword();

        if (!this.passwordEncoder.matches(signInUser.getPassword(), encodedPassword))
            throw new ApiException(500, ApiExceptionEnum.API_500_BADPASSWORD);

        return optionalUser.get();
    }

    public ApiUser loadUserByUsername(String username) throws ApiException {
        Optional<ApiUser> optionalApiUser = this.repositoryUser.findAllByEmail(username).stream().findFirst();

        if (optionalApiUser.isEmpty())
            throw new ApiException(500, ApiExceptionEnum.API_500_USERNOTFOUND);

        return optionalApiUser.get();
    }

    public Boolean checkIfChampionIsfavorite(ApiUser apiUser, String championId) {
        Optional<FavoriteChampion> optionalFavoriteChampion = this.repositoryFavoriteChampion.findFirstByUserAndAndChampionId(apiUser, championId);

        return optionalFavoriteChampion.isPresent();
    }

    public void changeFavoriteChampion(ApiUser apiUser, ChangeFavoriteChampion changeFavoriteChampion) {
        if (changeFavoriteChampion.getAddToFavorite()) {
            FavoriteChampion favoriteChampion = new FavoriteChampion(apiUser, changeFavoriteChampion.getChampionId());
            this.repositoryFavoriteChampion.saveAndFlush(favoriteChampion);
        } else {
            this.repositoryFavoriteChampion.findFirstByUserAndAndChampionId(
                    apiUser,
                    changeFavoriteChampion.getChampionId()
            ).ifPresent(this.repositoryFavoriteChampion::delete);
        }
    }

    public List<Champion> getFavoriteChampionIdByApiUser(ApiUser apiUser, List<Champion> allChampionFromRiot) {
        List<String> championIdList = this.repositoryFavoriteChampion.findAllByUser(apiUser)
                .stream()
                .map(FavoriteChampion::getChampionId)
                .collect(Collectors.toList());

        return allChampionFromRiot.stream()
                .filter(champion -> championIdList.contains(champion.getName()))
                .collect(Collectors.toList());
    }

    public Boolean checkIfSummonerIsfavorite(ApiUser apiUser, String summonerName) {
        Optional<FavoriteSummoner> optionalFavoriteSummoner = this.repositoryFavoriteSummoner.findFirstByUserAndAndSummonerName(apiUser, summonerName);

        return optionalFavoriteSummoner.isPresent();
    }

    public void changeFavoriteSummoner(ApiUser apiUser, ChangeFavoriteSummoner changeFavoriteSummoner) {
        if (changeFavoriteSummoner.getAddToFavorite()) {
            FavoriteSummoner favoriteSummoner = new FavoriteSummoner(apiUser, changeFavoriteSummoner.getSummonerName(), changeFavoriteSummoner.getSummonerIcon());
            this.repositoryFavoriteSummoner.saveAndFlush(favoriteSummoner);
        } else {
            this.repositoryFavoriteSummoner.findFirstByUserAndAndSummonerName(
                    apiUser,
                    changeFavoriteSummoner.getSummonerName()
            ).ifPresent(this.repositoryFavoriteSummoner::delete);
        }
    }

    public List<SummonerLight> getFavoriteSummonerByApiUser(ApiUser apiUser) {
        return this.repositoryFavoriteSummoner.findAllByUser(apiUser)
                .stream()
                .map(favoriteSummoner -> new SummonerLight(favoriteSummoner.getSummonerName(), favoriteSummoner.getIconeId()))
                .collect(Collectors.toList());
    }
}
