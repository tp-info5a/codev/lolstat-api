package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "user_password")
public class ApiUserPassword {

    @Id
    @Column(name = "user_id")
    private Long id;

    @OneToOne()
    @MapsId
    @JoinColumn(name = "user_id")
    private ApiUser user;

    private String password;

    public ApiUserPassword(ApiUser user, String password) {
        this.user = user;
        this.password = password;
    }
}
