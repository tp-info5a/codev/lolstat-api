package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import polytech.codev.lolstat_api.utils.models.RegisterUser;

import javax.persistence.*;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "user")
public class ApiUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private ApiUserPassword userPassword;

    @OneToMany(mappedBy = "user")
    private List<FavoriteChampion> favoriteChampionList;

    public ApiUser(RegisterUser registerUser) {
        this.firstName = registerUser.getFirstName();
        this.lastName = registerUser.getLastName();
        this.email = registerUser.getEmail();
        this.userPassword = new ApiUserPassword(this, registerUser.getPassword());
    }
}
