package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChampionDataInfo {
    private Integer attack;
    private Integer defence;
    private Integer magic;
    private Integer difficulty;

    public ChampionDataInfo() {}
}
