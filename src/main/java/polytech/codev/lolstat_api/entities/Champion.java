package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
//@Entity
//@Table(name = "champion")
public class Champion {
    //@Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private String name;
    private String imageSquare;
    private Boolean free;

    public Champion() {}

    public Champion(Integer id, String name, String imageSquare, Boolean free) {
        this.id = id;
        this.name = name;
        this.imageSquare = imageSquare;
        this.free = free;
    }

    public Champion(Integer id, String name, String imageSquare) {
        this(id, name, imageSquare, false);
    }
}
