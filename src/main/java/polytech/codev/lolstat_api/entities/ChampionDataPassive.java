package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChampionDataPassive {
    private String name;
    private String description;
    private String imageUrl;

    public ChampionDataPassive() {}
}
