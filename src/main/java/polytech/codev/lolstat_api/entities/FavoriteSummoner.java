package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "favorite_summoner")
public class FavoriteSummoner {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private ApiUser user;

    @Column(name = "summoner_name", nullable = false)
    private String summonerName;

    @Column(name = "icone_id", nullable = false)
    private Integer iconeId;

    public FavoriteSummoner(ApiUser apiUser, String summonerName, Integer iconeId) {
        this.user = apiUser;
        this.summonerName = summonerName;
        this.iconeId = iconeId;
    }
}
