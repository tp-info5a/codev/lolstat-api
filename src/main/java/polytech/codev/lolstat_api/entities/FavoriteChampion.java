package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "favorite_champion")
public class FavoriteChampion {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private ApiUser user;

    @Column(name = "champion_id", nullable = false)
    private String championId;

    public FavoriteChampion(ApiUser apiUser, String championId) {
        this.user = apiUser;
        this.championId = championId;
    }
}
