package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ChampionDataSkin {
    private Integer num;
    private String name;
    private String imageUrl;
}
