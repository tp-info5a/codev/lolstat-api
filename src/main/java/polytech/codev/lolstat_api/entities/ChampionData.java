package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChampionData {
    private String id;
    private Integer key;
    private String name;
    private String imageUrl;
    private String title;
    private String lore;
    private List<String> allytips;
    private List<String> enemytips;
    private List<String> tags;
    private String partype;
    private ChampionDataInfo info;
    private ChampionDataPassive passive;
    private List<ChampionDataSpell> spells;
    private List<ItemForChampion> recommended;
    private List<ChampionDataSkin> skins;


    public ChampionData() {}
}
