package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class PlayerDto {
    Integer profileIcon;
    String accountId;
    String matchHistoryUri;
    String currentAccountId;
    String currentPlatformId;
    String summonerName;
    String summonerId;
    String platformId;
}
