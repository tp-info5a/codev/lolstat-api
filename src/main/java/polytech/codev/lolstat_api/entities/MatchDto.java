package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class MatchDto {
    Long gameId;
    List<ParticipantIdentityDto> participantIdentities;
    Integer queue;
    String gameType;
    Long gameDuration;
    List<TeamStatsDto> teams;
    String platformId;
    Long gameCreation;
    Integer seasonId;
    String gameVersion;
    Integer mapId;
    String gameMode;
    List<ParticipantDto> participants;
}
