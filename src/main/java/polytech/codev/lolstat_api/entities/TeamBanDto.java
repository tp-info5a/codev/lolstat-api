package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class TeamBanDto {
    Integer championId;
    Integer pickTurn;
}
