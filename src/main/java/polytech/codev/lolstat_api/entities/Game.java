package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class Game {
    String platformId;
    Long gameId;
    Integer champion;
    Integer queue;
    Integer season;
    Long timestamp;
    String role;
    String lane;
}
