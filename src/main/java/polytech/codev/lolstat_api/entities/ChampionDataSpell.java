package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ChampionDataSpell {
    private String id;
    private String name;
    private String description;
    private String tooltip;
    private String cooldownBurn;
    private String costBurn;
    private List<String> effectBurn;
    // private List<ChampionDataSpellVar> vars;
    private String rangeBurn;
    private String imageUrl;

    public ChampionDataSpell() {}
}
