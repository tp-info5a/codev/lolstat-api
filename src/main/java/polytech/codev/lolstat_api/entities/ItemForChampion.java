package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class ItemForChampion {
    private String map;
    private String mode;
    private String type;
    private List<ItemBlock> blocks;
}
