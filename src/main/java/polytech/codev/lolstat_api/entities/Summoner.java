package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.Setter;

import java.math.BigInteger;

@Getter
@Setter
public class Summoner {

    private String id;
    private String accountId;
    private String puuid;
    private String name;
    private Integer profileIconId;
    private BigInteger revisionDate;
    private Integer summonerLevel;

    public Summoner() {}
}
