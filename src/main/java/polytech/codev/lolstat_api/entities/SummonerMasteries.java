package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import polytech.codev.lolstat_api.utils.models.SummonerMasteriesUtils;

import java.math.BigInteger;

@NoArgsConstructor
@Getter
@Setter
public class SummonerMasteries {
    private Champion champion;
    private Integer championLevel;
    private Integer championPoints;
    private BigInteger lastPlayTime;
    private Integer championPointsSinceLastLevel;
    private Integer championPointsUntilNextLevel;
    private Boolean chestGranted;
    private Integer tokensEarned;
    private String summonerId;

    public SummonerMasteries(Champion champion, SummonerMasteriesUtils summonerMasteriesUtils) {
        this.champion = champion;
        this.championLevel = summonerMasteriesUtils.getChampionLevel();
        this.championPoints = summonerMasteriesUtils.getChampionPoints();
        this.lastPlayTime = summonerMasteriesUtils.getLastPlayTime();
        this.championPointsSinceLastLevel = summonerMasteriesUtils.getChampionPointsSinceLastLevel();
        this.championPointsUntilNextLevel = summonerMasteriesUtils.getChampionPointsUntilNextLevel();
        this.chestGranted = summonerMasteriesUtils.getChestGranted();
        this.tokensEarned = summonerMasteriesUtils.getTokensEarned();
        this.summonerId = summonerMasteriesUtils.getSummonerId();
    }
}
