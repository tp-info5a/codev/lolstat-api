package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class ParticipantDto {
    Integer participantId;
    Integer championId;
    List<RuneDto> runes;
    ParticipantStatsDto stats;
    Integer teamId;
    ParticipantTimelineDto timeline;
    Integer spell1Id;
    Integer spell2Id;
    String highestAchievedSeasonTier;
    List<MasteryDto> masteries;

}
