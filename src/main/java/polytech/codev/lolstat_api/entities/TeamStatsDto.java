package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class TeamStatsDto {
    Integer towerKills;
    Integer riftHeraldKills;
    Boolean firstBlood;
    Integer inhibitorKills;
    List<TeamBanDto> bans;
    Boolean firstBaron;
    Boolean firstDragon;
    Integer dominionVictoryScore;
    Integer dragonKills;
    Integer baronKills;
    Boolean firstInhibitor;
    Boolean firstTower;
    Integer vileMawKills;
    Boolean firstRiftHerald;
    Integer teamId;
    String win;
}
