package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
public class ParticipantTimelineDto {
    Integer participantId;
    Map<String,Double> csDiffPerMinDeltas;
    Map<String,Double> damageTakenPerMinDeltas;
    String role;
    Map<String,Double> damageTakenDiffPerMinDeltas;
    Map<String,Double> xpPerMinDeltas;
    Map<String,Double> xpDiffPerMinDeltas;
    String lane;
    Map<String,Double> creepsPerMinDeltas;
    Map<String,Double> goldPerMinDeltas;
}
