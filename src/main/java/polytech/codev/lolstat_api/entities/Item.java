package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Item {
    private Integer id;
    private String name;
    private String description;
    private ItemGold gold;
    private String imageUrl;

    public void completeItem(Item item) {
        this.name = item.name;
        this.description = item.description;
        this.gold = item.gold;
    }
}
