package polytech.codev.lolstat_api.entities;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChampionDataSpellVar {
    private String link;
    private Double coeff;
    private String key;

    public ChampionDataSpellVar() {}
}
