package polytech.codev.lolstat_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import polytech.codev.lolstat_api.entities.ApiUser;
import polytech.codev.lolstat_api.entities.FavoriteChampion;

import java.util.List;
import java.util.Optional;

public interface RepositoryFavoriteChampion extends JpaRepository<FavoriteChampion, Long> {
    Optional<FavoriteChampion> findFirstByUserAndAndChampionId(ApiUser apiUser, String championId);

    List<FavoriteChampion> findAllByUser(ApiUser apiUser);
}
