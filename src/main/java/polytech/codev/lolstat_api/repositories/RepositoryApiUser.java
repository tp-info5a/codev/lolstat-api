package polytech.codev.lolstat_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import polytech.codev.lolstat_api.entities.ApiUser;

import java.util.List;

@Repository
public interface RepositoryApiUser extends JpaRepository<ApiUser, Long> {

    List<ApiUser> findAllByEmail(String email);
}
