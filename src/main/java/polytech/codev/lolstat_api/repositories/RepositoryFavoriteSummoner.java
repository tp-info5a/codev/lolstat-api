package polytech.codev.lolstat_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import polytech.codev.lolstat_api.entities.ApiUser;
import polytech.codev.lolstat_api.entities.FavoriteSummoner;

import java.util.List;
import java.util.Optional;

public interface RepositoryFavoriteSummoner extends JpaRepository<FavoriteSummoner, Long> {
    Optional<FavoriteSummoner> findFirstByUserAndAndSummonerName(ApiUser apiUser, String summonerName);

    List<FavoriteSummoner> findAllByUser(ApiUser apiUser);
}
