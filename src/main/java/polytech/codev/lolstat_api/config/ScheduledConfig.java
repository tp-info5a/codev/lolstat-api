package polytech.codev.lolstat_api.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class ScheduledConfig {
    private final Logger logger = LoggerFactory.getLogger(ScheduledConfig.class);

    @CacheEvict(value = {"allChampions", "freeChampionRotation"})
    @Scheduled(cron = "0 0 8 ? * TUE")
    public void flushCache() {
        this.logger.info("cache flushed");
    }
}
