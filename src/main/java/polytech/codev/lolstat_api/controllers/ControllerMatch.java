package polytech.codev.lolstat_api.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.entities.Item;
import polytech.codev.lolstat_api.entities.MatchDto;
import polytech.codev.lolstat_api.utils.WebDataRiot;
import polytech.codev.lolstat_api.utils.WebServiceRiot;

@Controller
@RequestMapping("/api/match")
public class ControllerMatch {

    private final WebServiceRiot webServiceRiot;
    private final WebDataRiot webDataRiot;

    public ControllerMatch(WebServiceRiot webServiceRiot, WebDataRiot webDataRiot) {
        this.webServiceRiot = webServiceRiot;
        this.webDataRiot = webDataRiot;
    }

    @RequestMapping(value = "detail/{id}", method = RequestMethod.GET)
    public ResponseEntity<MatchDto> getInfo(@PathVariable("id") Long id) throws ApiException {
        return ResponseEntity.ok(this.webServiceRiot.getInfoMatch(id));
    }

    @RequestMapping(value = "/get-item/{id}", method = RequestMethod.GET)
    public ResponseEntity<Item> getItem(@PathVariable("id") Integer id) throws ApiException {
        return ResponseEntity.ok(this.webDataRiot.getItemById(id));
    }

    @RequestMapping(value = "/get-map/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getMap(@PathVariable("id") Integer id) throws ApiException {
        String mapById = this.webDataRiot.getMapById(id);
        return ResponseEntity.ok("{ \"mapName\": \"" + mapById + "\"}");
    }
}
