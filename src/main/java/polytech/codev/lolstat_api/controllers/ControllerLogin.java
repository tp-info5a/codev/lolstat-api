package polytech.codev.lolstat_api.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.entities.ApiUser;
import polytech.codev.lolstat_api.utils.jwt.JwtUtil;
import polytech.codev.lolstat_api.utils.models.SignInUser;
import polytech.codev.lolstat_api.utils.models.RegisterUser;
import polytech.codev.lolstat_api.services.ServiceUser;

@Controller
@RequestMapping("/api/login")
public class ControllerLogin {

    private ServiceUser serviceUser;
    private JwtUtil jwtUtil;

    public ControllerLogin(ServiceUser serviceUser, JwtUtil jwtUtil) {
        this.serviceUser = serviceUser;
        this.jwtUtil = jwtUtil;
    }

    @RequestMapping(value = "/new-user", method = RequestMethod.POST)
    public ResponseEntity<String> newUser(@RequestBody RegisterUser registerUser) throws ApiException {
        ApiUser apiUser = this.serviceUser.registerNewUser(registerUser);
        UserDetails userDetails = this.jwtUtil.loadUser(apiUser);
        String token = this.jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok("{ \"token\": \"" + token + "\"}");
    }

    @RequestMapping(value = "/sign-in-user", method = RequestMethod.POST)
    public ResponseEntity<String> signInUSer(@RequestBody SignInUser signInUser) throws ApiException {
        ApiUser apiUser = this.serviceUser.signInUser(signInUser);
        UserDetails userDetails = this.jwtUtil.loadUser(apiUser);
        String token = this.jwtUtil.generateToken(userDetails);

        return ResponseEntity.ok("{ \"token\": \"" + token + "\"}");
    }
}
