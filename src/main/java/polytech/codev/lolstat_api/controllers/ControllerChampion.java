package polytech.codev.lolstat_api.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.entities.Champion;
import polytech.codev.lolstat_api.entities.ChampionData;
import polytech.codev.lolstat_api.services.ServiceChampion;
import polytech.codev.lolstat_api.utils.WebDataRiot;
import polytech.codev.lolstat_api.utils.WebServiceRiot;

import java.util.List;

@Controller
@RequestMapping("/api/champion")
public class ControllerChampion {

    private final WebDataRiot webDataRiot;
    private final WebServiceRiot webServiceRiot;

    private final ServiceChampion serviceChampion;

    public ControllerChampion(WebDataRiot webDataRiot, WebServiceRiot webServiceRiot, ServiceChampion serviceChampion) {
        this.webDataRiot = webDataRiot;
        this.webServiceRiot = webServiceRiot;
        this.serviceChampion = serviceChampion;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ResponseEntity<List<Champion>> getAllChampion() throws ApiException {
        List<Champion> allChampionFromRiot = this.webDataRiot.getAllChampionFromRiot();
        List<Integer> freeChampionId = this.webServiceRiot.getFreeChampionId();
        this.serviceChampion.getFreeChampionFromIdList(allChampionFromRiot, freeChampionId);
        return ResponseEntity.ok(allChampionFromRiot);
    }

    @RequestMapping(value = "/by-name/{championName}", method = RequestMethod.GET)
    public ResponseEntity<ChampionData> getChampionByName(@PathVariable("championName") String championName) throws ApiException {
        ChampionData championInfo = this.webDataRiot.getChampionByName(championName);
        return ResponseEntity.ok(championInfo);
    }

    @RequestMapping(value = "/by-id/{championId}", method = RequestMethod.GET)
    public ResponseEntity<Champion> getChampionById(@PathVariable("championId") Integer championId) throws ApiException {
        Champion championInfo = this.webDataRiot.getChampionById(championId);
        return ResponseEntity.ok(championInfo);
    }
}
