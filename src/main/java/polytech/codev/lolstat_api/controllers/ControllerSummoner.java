package polytech.codev.lolstat_api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.entities.*;
import polytech.codev.lolstat_api.utils.WebDataRiot;
import polytech.codev.lolstat_api.utils.WebServiceRiot;

import java.util.List;

@Controller
@RequestMapping("/api/summoner")
public class ControllerSummoner {

    private final WebServiceRiot webServiceRiot;
    private final WebDataRiot webDataRiot;

    public ControllerSummoner(WebServiceRiot webServiceRiot, WebDataRiot webDataRiot) {
        this.webServiceRiot = webServiceRiot;
        this.webDataRiot = webDataRiot;
    }

    @RequestMapping(value = "/get-by-name/{name}", method = RequestMethod.GET)
    public ResponseEntity<Summoner> getSummonerByName(@PathVariable("name") String name) throws ApiException {
        return ResponseEntity.ok(this.webServiceRiot.getSummonerByName(name));
    }

    @RequestMapping(value = "/get-mastery/{summonerId}", method = RequestMethod.GET)
    public ResponseEntity<List<SummonerMasteries>> getMasteriesById(@PathVariable("summonerId") String summonerId) throws ApiException {
        List<Champion> allChampionFromRiot = this.webDataRiot.getAllChampionFromRiot();
        return ResponseEntity.ok(this.webServiceRiot.getMasteryBySummonerId(summonerId, allChampionFromRiot));
    }

    @RequestMapping(value = "/get-history/{accountId}/{page}", method = RequestMethod.GET)
    public ResponseEntity<Page<Game>> getHistoryByAccountId(@PathVariable("accountId") String accountId, @PathVariable("page") Integer page)throws ApiException{
        return ResponseEntity.ok(this.webServiceRiot.getHistoryByAccountId(accountId, page));
    }
}
