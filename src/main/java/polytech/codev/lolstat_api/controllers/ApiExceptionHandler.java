package polytech.codev.lolstat_api.controllers;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import polytech.codev.lolstat_api.Exceptions.ApiException;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ApiException.class)
    protected ResponseEntity<Object> handleEntityNotFound(ApiException e) {
        HttpStatus httpStatus = HttpStatus.resolve(e.getStatus()) != null ? HttpStatus.resolve(e.getStatus()) : HttpStatus.INTERNAL_SERVER_ERROR;
        assert httpStatus != null;
        return new ResponseEntity<>(e, httpStatus);
    }
}
