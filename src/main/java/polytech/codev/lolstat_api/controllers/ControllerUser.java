package polytech.codev.lolstat_api.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import polytech.codev.lolstat_api.Exceptions.ApiException;
import polytech.codev.lolstat_api.entities.ApiUser;
import polytech.codev.lolstat_api.entities.Champion;
import polytech.codev.lolstat_api.entities.SummonerLight;
import polytech.codev.lolstat_api.services.ServiceUser;
import polytech.codev.lolstat_api.utils.WebDataRiot;
import polytech.codev.lolstat_api.utils.models.ChangeFavoriteChampion;
import polytech.codev.lolstat_api.utils.models.ChangeFavoriteSummoner;

import java.util.List;

@Controller
@RequestMapping("/api/user")
public class ControllerUser {

    private final ServiceUser serviceUser;
    private final WebDataRiot webDataRiot;

    public ControllerUser(ServiceUser serviceUser, WebDataRiot webDataRiot) {
        this.serviceUser = serviceUser;
        this.webDataRiot = webDataRiot;
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public ResponseEntity<String> hello() {
        return ResponseEntity.ok("{ \"string\" : \"hello world !\", }");
    }

    @RequestMapping(value = "/check-favorite-champion", method = RequestMethod.POST)
    public ResponseEntity<Boolean> isChampionFavorite(@RequestBody String championId) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.serviceUser.loadUserByUsername(username);

        return ResponseEntity.ok(this.serviceUser.checkIfChampionIsfavorite(apiUser, championId));
    }

    @RequestMapping(value = "change-favorite-champion", method = RequestMethod.POST)
    public ResponseEntity<Boolean> changeFavoriteChampion(@RequestBody ChangeFavoriteChampion changeFavoriteChampion) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.serviceUser.loadUserByUsername(username);

        this.serviceUser.changeFavoriteChampion(apiUser, changeFavoriteChampion);

        return ResponseEntity.ok(Boolean.TRUE);
    }

    @RequestMapping(value = "/get-favorite-champions", method = RequestMethod.GET)
    public ResponseEntity<List<Champion>> getFavoriteChampionList() throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.serviceUser.loadUserByUsername(username);
        List<Champion> allChampionFromRiot = this.webDataRiot.getAllChampionFromRiot();
        List<Champion> favoriteChampionList = this.serviceUser.getFavoriteChampionIdByApiUser(apiUser, allChampionFromRiot);

        return ResponseEntity.ok(favoriteChampionList);
    }

    @RequestMapping(value = "/check-favorite-summoner", method = RequestMethod.POST)
    public ResponseEntity<Boolean> isSummonerFavorite(@RequestBody String summonerName) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.serviceUser.loadUserByUsername(username);

        return ResponseEntity.ok(this.serviceUser.checkIfSummonerIsfavorite(apiUser, summonerName));
    }

    @RequestMapping(value = "change-favorite-summoner", method = RequestMethod.POST)
    public ResponseEntity<Boolean> changeFavoriteSummoner(@RequestBody ChangeFavoriteSummoner changeFavoriteSummoner) throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.serviceUser.loadUserByUsername(username);

        this.serviceUser.changeFavoriteSummoner(apiUser, changeFavoriteSummoner);

        return ResponseEntity.ok(Boolean.TRUE);
    }

    @RequestMapping(value = "/get-favorite-summoner", method = RequestMethod.GET)
    public ResponseEntity<List<SummonerLight>> getFavoriteSummonerList() throws ApiException {
        String username = ((User) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        ApiUser apiUser = this.serviceUser.loadUserByUsername(username);
        List<SummonerLight> favoriteSummonerByApiUser = this.serviceUser.getFavoriteSummonerByApiUser(apiUser);

        return ResponseEntity.ok(favoriteSummonerByApiUser);
    }
}
