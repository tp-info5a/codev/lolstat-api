# Cahier des charges (lolstat)

> Martin CAZET (martin.cazet@etu.univ-lyon1.fr) <br>
> Nathan ARMANET (nathan.armanet@etu.univ-lyon1.fr)

## Application

L'application créée sera une interface de support où les joueurs de League of Legends pourront voir les statistiques (matchs joués, maîtrise des champions, etc).
Il pourront aussi rechercher d'autres joueurs et voir leurs statisques.
Des informations sur les champions seront aussi disponible tel que :
- la rotation des champions gratuits
- les compétences d'un champion
- les runes & objets recommandéesses
- les différents skins

### Fonctionnalités

Voici la listes des fonctionnalités que nous pensons implémenter dans notre application :

- authentification des users
- win rate des champions
- historique des matchs d'un joueur
- liste des champions
- informations sur un champion spécifique
- recherche d'autres joueurs

## Technologies

Client : angular <br>
Serveur : Spring boot <br>
base de données : mysql <br>
openData source : developer.riotgames.com

JDK : adopopenjdk11